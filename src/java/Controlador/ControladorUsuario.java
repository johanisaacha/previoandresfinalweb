/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controlador;

import entidades.Usuarios;
import entidadesDAO.RolJpaController;
import entidadesDAO.UsuariosJpaController;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import negocio.LoginUsuario;

/**
 *
 * @author johan.alvarez_pragma
 */
@WebServlet(name = "ControladorUsuario", urlPatterns = {"/ControladorUsuario"})
public class ControladorUsuario extends HttpServlet {

    String agregar = "vistas/agregar.jsp";
    String index = "index.jsp";
    Usuarios usuario = new Usuarios();
    UsuariosJpaController usuarioDAO = new UsuariosJpaController();
    RolJpaController rolDAO = new RolJpaController();
    LoginUsuario login = new LoginUsuario();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControladorUsuario</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControladorUsuario at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String acceso = "";
        String action = request.getParameter("accion");
        
        if (action.equalsIgnoreCase("inicioSesion")){
            String email = request.getParameter("username");
            String password = request.getParameter("password");
            if (login.inicioSesion(email, password)) {
                acceso = agregar;
            } else {
                acceso = index;
            }
        } else if (action.equalsIgnoreCase("nuevo")) {
            usuario.setTipoDocumento(request.getParameter("txtTipoDocumento"));
            usuario.setNumeroDocumento(request.getParameter("txtNumeroDocumento"));
            usuario.setNombre(request.getParameter("txtNombre"));
            usuario.setApellido(request.getParameter("txtApellido"));
            usuario.setEmail(request.getParameter("txtCorreo"));
            usuario.setTelefono(request.getParameter("txtTelefono"));
            usuario.setPassword(request.getParameter("txtPassword"));
            usuario.setIdRol(rolDAO.findRol(Integer.valueOf(request.getParameter("txtRol"))));

            try {
                usuarioDAO.create(usuario);
            } catch (Exception ex) {
                Logger.getLogger(ControladorUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
            acceso = agregar;
        }

        RequestDispatcher vista = request.getRequestDispatcher(acceso);
        vista.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
