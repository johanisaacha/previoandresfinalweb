/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author johan.alvarez_pragma
 */
@Entity
@Table(name = "opcion_menu")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OpcionMenu.findAll", query = "SELECT o FROM OpcionMenu o"),
    @NamedQuery(name = "OpcionMenu.findById", query = "SELECT o FROM OpcionMenu o WHERE o.id = :id"),
    @NamedQuery(name = "OpcionMenu.findByNombre", query = "SELECT o FROM OpcionMenu o WHERE o.nombre = :nombre"),
    @NamedQuery(name = "OpcionMenu.findByUrl", query = "SELECT o FROM OpcionMenu o WHERE o.url = :url"),
    @NamedQuery(name = "OpcionMenu.findByIcono", query = "SELECT o FROM OpcionMenu o WHERE o.icono = :icono")})
public class OpcionMenu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "url")
    private String url;
    @Basic(optional = false)
    @Column(name = "icono")
    private String icono;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idOpcionMenu")
    private Collection<RolOpcionMenu> rolOpcionMenuCollection;

    public OpcionMenu() {
    }

    public OpcionMenu(Integer id) {
        this.id = id;
    }

    public OpcionMenu(Integer id, String nombre, String url, String icono) {
        this.id = id;
        this.nombre = nombre;
        this.url = url;
        this.icono = icono;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    @XmlTransient
    public Collection<RolOpcionMenu> getRolOpcionMenuCollection() {
        return rolOpcionMenuCollection;
    }

    public void setRolOpcionMenuCollection(Collection<RolOpcionMenu> rolOpcionMenuCollection) {
        this.rolOpcionMenuCollection = rolOpcionMenuCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OpcionMenu)) {
            return false;
        }
        OpcionMenu other = (OpcionMenu) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.OpcionMenu[ id=" + id + " ]";
    }
    
}
