/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author johan.alvarez_pragma
 */
@Entity
@Table(name = "rol_opcion_menu")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RolOpcionMenu.findAll", query = "SELECT r FROM RolOpcionMenu r"),
    @NamedQuery(name = "RolOpcionMenu.findById", query = "SELECT r FROM RolOpcionMenu r WHERE r.id = :id")})
public class RolOpcionMenu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "id_rol", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Rol idRol;
    @JoinColumn(name = "id_opcion_menu", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private OpcionMenu idOpcionMenu;

    public RolOpcionMenu() {
    }

    public RolOpcionMenu(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Rol getIdRol() {
        return idRol;
    }

    public void setIdRol(Rol idRol) {
        this.idRol = idRol;
    }

    public OpcionMenu getIdOpcionMenu() {
        return idOpcionMenu;
    }

    public void setIdOpcionMenu(OpcionMenu idOpcionMenu) {
        this.idOpcionMenu = idOpcionMenu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolOpcionMenu)) {
            return false;
        }
        RolOpcionMenu other = (RolOpcionMenu) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.RolOpcionMenu[ id=" + id + " ]";
    }
    
}
