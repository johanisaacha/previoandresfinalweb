/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entidadesDAO;

import entidades.OpcionMenu;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entidades.RolOpcionMenu;
import entidadesDAO.exceptions.IllegalOrphanException;
import entidadesDAO.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author johan.alvarez_pragma
 */
public class OpcionMenuJpaController implements Serializable {

    public OpcionMenuJpaController() {
    }   

    public EntityManager getEntityManager() {
        return Persistence.createEntityManagerFactory("SisGestionUsuariosPU")
                                     .createEntityManager();
    }

    public void create(OpcionMenu opcionMenu) {
        if (opcionMenu.getRolOpcionMenuCollection() == null) {
            opcionMenu.setRolOpcionMenuCollection(new ArrayList<RolOpcionMenu>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<RolOpcionMenu> attachedRolOpcionMenuCollection = new ArrayList<RolOpcionMenu>();
            for (RolOpcionMenu rolOpcionMenuCollectionRolOpcionMenuToAttach : opcionMenu.getRolOpcionMenuCollection()) {
                rolOpcionMenuCollectionRolOpcionMenuToAttach = em.getReference(rolOpcionMenuCollectionRolOpcionMenuToAttach.getClass(), rolOpcionMenuCollectionRolOpcionMenuToAttach.getId());
                attachedRolOpcionMenuCollection.add(rolOpcionMenuCollectionRolOpcionMenuToAttach);
            }
            opcionMenu.setRolOpcionMenuCollection(attachedRolOpcionMenuCollection);
            em.persist(opcionMenu);
            for (RolOpcionMenu rolOpcionMenuCollectionRolOpcionMenu : opcionMenu.getRolOpcionMenuCollection()) {
                OpcionMenu oldIdOpcionMenuOfRolOpcionMenuCollectionRolOpcionMenu = rolOpcionMenuCollectionRolOpcionMenu.getIdOpcionMenu();
                rolOpcionMenuCollectionRolOpcionMenu.setIdOpcionMenu(opcionMenu);
                rolOpcionMenuCollectionRolOpcionMenu = em.merge(rolOpcionMenuCollectionRolOpcionMenu);
                if (oldIdOpcionMenuOfRolOpcionMenuCollectionRolOpcionMenu != null) {
                    oldIdOpcionMenuOfRolOpcionMenuCollectionRolOpcionMenu.getRolOpcionMenuCollection().remove(rolOpcionMenuCollectionRolOpcionMenu);
                    oldIdOpcionMenuOfRolOpcionMenuCollectionRolOpcionMenu = em.merge(oldIdOpcionMenuOfRolOpcionMenuCollectionRolOpcionMenu);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(OpcionMenu opcionMenu) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OpcionMenu persistentOpcionMenu = em.find(OpcionMenu.class, opcionMenu.getId());
            Collection<RolOpcionMenu> rolOpcionMenuCollectionOld = persistentOpcionMenu.getRolOpcionMenuCollection();
            Collection<RolOpcionMenu> rolOpcionMenuCollectionNew = opcionMenu.getRolOpcionMenuCollection();
            List<String> illegalOrphanMessages = null;
            for (RolOpcionMenu rolOpcionMenuCollectionOldRolOpcionMenu : rolOpcionMenuCollectionOld) {
                if (!rolOpcionMenuCollectionNew.contains(rolOpcionMenuCollectionOldRolOpcionMenu)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain RolOpcionMenu " + rolOpcionMenuCollectionOldRolOpcionMenu + " since its idOpcionMenu field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<RolOpcionMenu> attachedRolOpcionMenuCollectionNew = new ArrayList<RolOpcionMenu>();
            for (RolOpcionMenu rolOpcionMenuCollectionNewRolOpcionMenuToAttach : rolOpcionMenuCollectionNew) {
                rolOpcionMenuCollectionNewRolOpcionMenuToAttach = em.getReference(rolOpcionMenuCollectionNewRolOpcionMenuToAttach.getClass(), rolOpcionMenuCollectionNewRolOpcionMenuToAttach.getId());
                attachedRolOpcionMenuCollectionNew.add(rolOpcionMenuCollectionNewRolOpcionMenuToAttach);
            }
            rolOpcionMenuCollectionNew = attachedRolOpcionMenuCollectionNew;
            opcionMenu.setRolOpcionMenuCollection(rolOpcionMenuCollectionNew);
            opcionMenu = em.merge(opcionMenu);
            for (RolOpcionMenu rolOpcionMenuCollectionNewRolOpcionMenu : rolOpcionMenuCollectionNew) {
                if (!rolOpcionMenuCollectionOld.contains(rolOpcionMenuCollectionNewRolOpcionMenu)) {
                    OpcionMenu oldIdOpcionMenuOfRolOpcionMenuCollectionNewRolOpcionMenu = rolOpcionMenuCollectionNewRolOpcionMenu.getIdOpcionMenu();
                    rolOpcionMenuCollectionNewRolOpcionMenu.setIdOpcionMenu(opcionMenu);
                    rolOpcionMenuCollectionNewRolOpcionMenu = em.merge(rolOpcionMenuCollectionNewRolOpcionMenu);
                    if (oldIdOpcionMenuOfRolOpcionMenuCollectionNewRolOpcionMenu != null && !oldIdOpcionMenuOfRolOpcionMenuCollectionNewRolOpcionMenu.equals(opcionMenu)) {
                        oldIdOpcionMenuOfRolOpcionMenuCollectionNewRolOpcionMenu.getRolOpcionMenuCollection().remove(rolOpcionMenuCollectionNewRolOpcionMenu);
                        oldIdOpcionMenuOfRolOpcionMenuCollectionNewRolOpcionMenu = em.merge(oldIdOpcionMenuOfRolOpcionMenuCollectionNewRolOpcionMenu);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = opcionMenu.getId();
                if (findOpcionMenu(id) == null) {
                    throw new NonexistentEntityException("The opcionMenu with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OpcionMenu opcionMenu;
            try {
                opcionMenu = em.getReference(OpcionMenu.class, id);
                opcionMenu.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The opcionMenu with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<RolOpcionMenu> rolOpcionMenuCollectionOrphanCheck = opcionMenu.getRolOpcionMenuCollection();
            for (RolOpcionMenu rolOpcionMenuCollectionOrphanCheckRolOpcionMenu : rolOpcionMenuCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This OpcionMenu (" + opcionMenu + ") cannot be destroyed since the RolOpcionMenu " + rolOpcionMenuCollectionOrphanCheckRolOpcionMenu + " in its rolOpcionMenuCollection field has a non-nullable idOpcionMenu field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(opcionMenu);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<OpcionMenu> findOpcionMenuEntities() {
        return findOpcionMenuEntities(true, -1, -1);
    }

    public List<OpcionMenu> findOpcionMenuEntities(int maxResults, int firstResult) {
        return findOpcionMenuEntities(false, maxResults, firstResult);
    }

    private List<OpcionMenu> findOpcionMenuEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(OpcionMenu.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public OpcionMenu findOpcionMenu(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(OpcionMenu.class, id);
        } finally {
            em.close();
        }
    }

    public int getOpcionMenuCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<OpcionMenu> rt = cq.from(OpcionMenu.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
