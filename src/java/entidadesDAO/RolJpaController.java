/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entidadesDAO;

import entidades.Rol;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entidades.Usuarios;
import java.util.ArrayList;
import java.util.Collection;
import entidades.RolOpcionMenu;
import entidadesDAO.exceptions.IllegalOrphanException;
import entidadesDAO.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author johan.alvarez_pragma
 */
public class RolJpaController implements Serializable {

    public RolJpaController() {
    }  

    public EntityManager getEntityManager() {
        return Persistence.createEntityManagerFactory("SisGestionUsuariosPU")
                                     .createEntityManager();
    }

    public void create(Rol rol) {
        if (rol.getUsuariosCollection() == null) {
            rol.setUsuariosCollection(new ArrayList<Usuarios>());
        }
        if (rol.getRolOpcionMenuCollection() == null) {
            rol.setRolOpcionMenuCollection(new ArrayList<RolOpcionMenu>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Usuarios> attachedUsuariosCollection = new ArrayList<Usuarios>();
            for (Usuarios usuariosCollectionUsuariosToAttach : rol.getUsuariosCollection()) {
                usuariosCollectionUsuariosToAttach = em.getReference(usuariosCollectionUsuariosToAttach.getClass(), usuariosCollectionUsuariosToAttach.getNumeroDocumento());
                attachedUsuariosCollection.add(usuariosCollectionUsuariosToAttach);
            }
            rol.setUsuariosCollection(attachedUsuariosCollection);
            Collection<RolOpcionMenu> attachedRolOpcionMenuCollection = new ArrayList<RolOpcionMenu>();
            for (RolOpcionMenu rolOpcionMenuCollectionRolOpcionMenuToAttach : rol.getRolOpcionMenuCollection()) {
                rolOpcionMenuCollectionRolOpcionMenuToAttach = em.getReference(rolOpcionMenuCollectionRolOpcionMenuToAttach.getClass(), rolOpcionMenuCollectionRolOpcionMenuToAttach.getId());
                attachedRolOpcionMenuCollection.add(rolOpcionMenuCollectionRolOpcionMenuToAttach);
            }
            rol.setRolOpcionMenuCollection(attachedRolOpcionMenuCollection);
            em.persist(rol);
            for (Usuarios usuariosCollectionUsuarios : rol.getUsuariosCollection()) {
                Rol oldIdRolOfUsuariosCollectionUsuarios = usuariosCollectionUsuarios.getIdRol();
                usuariosCollectionUsuarios.setIdRol(rol);
                usuariosCollectionUsuarios = em.merge(usuariosCollectionUsuarios);
                if (oldIdRolOfUsuariosCollectionUsuarios != null) {
                    oldIdRolOfUsuariosCollectionUsuarios.getUsuariosCollection().remove(usuariosCollectionUsuarios);
                    oldIdRolOfUsuariosCollectionUsuarios = em.merge(oldIdRolOfUsuariosCollectionUsuarios);
                }
            }
            for (RolOpcionMenu rolOpcionMenuCollectionRolOpcionMenu : rol.getRolOpcionMenuCollection()) {
                Rol oldIdRolOfRolOpcionMenuCollectionRolOpcionMenu = rolOpcionMenuCollectionRolOpcionMenu.getIdRol();
                rolOpcionMenuCollectionRolOpcionMenu.setIdRol(rol);
                rolOpcionMenuCollectionRolOpcionMenu = em.merge(rolOpcionMenuCollectionRolOpcionMenu);
                if (oldIdRolOfRolOpcionMenuCollectionRolOpcionMenu != null) {
                    oldIdRolOfRolOpcionMenuCollectionRolOpcionMenu.getRolOpcionMenuCollection().remove(rolOpcionMenuCollectionRolOpcionMenu);
                    oldIdRolOfRolOpcionMenuCollectionRolOpcionMenu = em.merge(oldIdRolOfRolOpcionMenuCollectionRolOpcionMenu);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Rol rol) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Rol persistentRol = em.find(Rol.class, rol.getId());
            Collection<Usuarios> usuariosCollectionOld = persistentRol.getUsuariosCollection();
            Collection<Usuarios> usuariosCollectionNew = rol.getUsuariosCollection();
            Collection<RolOpcionMenu> rolOpcionMenuCollectionOld = persistentRol.getRolOpcionMenuCollection();
            Collection<RolOpcionMenu> rolOpcionMenuCollectionNew = rol.getRolOpcionMenuCollection();
            List<String> illegalOrphanMessages = null;
            for (Usuarios usuariosCollectionOldUsuarios : usuariosCollectionOld) {
                if (!usuariosCollectionNew.contains(usuariosCollectionOldUsuarios)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Usuarios " + usuariosCollectionOldUsuarios + " since its idRol field is not nullable.");
                }
            }
            for (RolOpcionMenu rolOpcionMenuCollectionOldRolOpcionMenu : rolOpcionMenuCollectionOld) {
                if (!rolOpcionMenuCollectionNew.contains(rolOpcionMenuCollectionOldRolOpcionMenu)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain RolOpcionMenu " + rolOpcionMenuCollectionOldRolOpcionMenu + " since its idRol field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Usuarios> attachedUsuariosCollectionNew = new ArrayList<Usuarios>();
            for (Usuarios usuariosCollectionNewUsuariosToAttach : usuariosCollectionNew) {
                usuariosCollectionNewUsuariosToAttach = em.getReference(usuariosCollectionNewUsuariosToAttach.getClass(), usuariosCollectionNewUsuariosToAttach.getNumeroDocumento());
                attachedUsuariosCollectionNew.add(usuariosCollectionNewUsuariosToAttach);
            }
            usuariosCollectionNew = attachedUsuariosCollectionNew;
            rol.setUsuariosCollection(usuariosCollectionNew);
            Collection<RolOpcionMenu> attachedRolOpcionMenuCollectionNew = new ArrayList<RolOpcionMenu>();
            for (RolOpcionMenu rolOpcionMenuCollectionNewRolOpcionMenuToAttach : rolOpcionMenuCollectionNew) {
                rolOpcionMenuCollectionNewRolOpcionMenuToAttach = em.getReference(rolOpcionMenuCollectionNewRolOpcionMenuToAttach.getClass(), rolOpcionMenuCollectionNewRolOpcionMenuToAttach.getId());
                attachedRolOpcionMenuCollectionNew.add(rolOpcionMenuCollectionNewRolOpcionMenuToAttach);
            }
            rolOpcionMenuCollectionNew = attachedRolOpcionMenuCollectionNew;
            rol.setRolOpcionMenuCollection(rolOpcionMenuCollectionNew);
            rol = em.merge(rol);
            for (Usuarios usuariosCollectionNewUsuarios : usuariosCollectionNew) {
                if (!usuariosCollectionOld.contains(usuariosCollectionNewUsuarios)) {
                    Rol oldIdRolOfUsuariosCollectionNewUsuarios = usuariosCollectionNewUsuarios.getIdRol();
                    usuariosCollectionNewUsuarios.setIdRol(rol);
                    usuariosCollectionNewUsuarios = em.merge(usuariosCollectionNewUsuarios);
                    if (oldIdRolOfUsuariosCollectionNewUsuarios != null && !oldIdRolOfUsuariosCollectionNewUsuarios.equals(rol)) {
                        oldIdRolOfUsuariosCollectionNewUsuarios.getUsuariosCollection().remove(usuariosCollectionNewUsuarios);
                        oldIdRolOfUsuariosCollectionNewUsuarios = em.merge(oldIdRolOfUsuariosCollectionNewUsuarios);
                    }
                }
            }
            for (RolOpcionMenu rolOpcionMenuCollectionNewRolOpcionMenu : rolOpcionMenuCollectionNew) {
                if (!rolOpcionMenuCollectionOld.contains(rolOpcionMenuCollectionNewRolOpcionMenu)) {
                    Rol oldIdRolOfRolOpcionMenuCollectionNewRolOpcionMenu = rolOpcionMenuCollectionNewRolOpcionMenu.getIdRol();
                    rolOpcionMenuCollectionNewRolOpcionMenu.setIdRol(rol);
                    rolOpcionMenuCollectionNewRolOpcionMenu = em.merge(rolOpcionMenuCollectionNewRolOpcionMenu);
                    if (oldIdRolOfRolOpcionMenuCollectionNewRolOpcionMenu != null && !oldIdRolOfRolOpcionMenuCollectionNewRolOpcionMenu.equals(rol)) {
                        oldIdRolOfRolOpcionMenuCollectionNewRolOpcionMenu.getRolOpcionMenuCollection().remove(rolOpcionMenuCollectionNewRolOpcionMenu);
                        oldIdRolOfRolOpcionMenuCollectionNewRolOpcionMenu = em.merge(oldIdRolOfRolOpcionMenuCollectionNewRolOpcionMenu);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = rol.getId();
                if (findRol(id) == null) {
                    throw new NonexistentEntityException("The rol with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Rol rol;
            try {
                rol = em.getReference(Rol.class, id);
                rol.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The rol with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Usuarios> usuariosCollectionOrphanCheck = rol.getUsuariosCollection();
            for (Usuarios usuariosCollectionOrphanCheckUsuarios : usuariosCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Rol (" + rol + ") cannot be destroyed since the Usuarios " + usuariosCollectionOrphanCheckUsuarios + " in its usuariosCollection field has a non-nullable idRol field.");
            }
            Collection<RolOpcionMenu> rolOpcionMenuCollectionOrphanCheck = rol.getRolOpcionMenuCollection();
            for (RolOpcionMenu rolOpcionMenuCollectionOrphanCheckRolOpcionMenu : rolOpcionMenuCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Rol (" + rol + ") cannot be destroyed since the RolOpcionMenu " + rolOpcionMenuCollectionOrphanCheckRolOpcionMenu + " in its rolOpcionMenuCollection field has a non-nullable idRol field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(rol);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Rol> findRolEntities() {
        return findRolEntities(true, -1, -1);
    }

    public List<Rol> findRolEntities(int maxResults, int firstResult) {
        return findRolEntities(false, maxResults, firstResult);
    }

    private List<Rol> findRolEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Rol.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Rol findRol(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Rol.class, id);
        } finally {
            em.close();
        }
    }

    public int getRolCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Rol> rt = cq.from(Rol.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
