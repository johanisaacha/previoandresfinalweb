/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entidadesDAO;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entidades.Rol;
import entidades.OpcionMenu;
import entidades.RolOpcionMenu;
import entidadesDAO.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author johan.alvarez_pragma
 */
public class RolOpcionMenuJpaController implements Serializable {

    public RolOpcionMenuJpaController() {
    }

    public EntityManager getEntityManager() {
        return Persistence.createEntityManagerFactory("SisGestionUsuariosPU")
                                     .createEntityManager();
    }

    public void create(RolOpcionMenu rolOpcionMenu) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Rol idRol = rolOpcionMenu.getIdRol();
            if (idRol != null) {
                idRol = em.getReference(idRol.getClass(), idRol.getId());
                rolOpcionMenu.setIdRol(idRol);
            }
            OpcionMenu idOpcionMenu = rolOpcionMenu.getIdOpcionMenu();
            if (idOpcionMenu != null) {
                idOpcionMenu = em.getReference(idOpcionMenu.getClass(), idOpcionMenu.getId());
                rolOpcionMenu.setIdOpcionMenu(idOpcionMenu);
            }
            em.persist(rolOpcionMenu);
            if (idRol != null) {
                idRol.getRolOpcionMenuCollection().add(rolOpcionMenu);
                idRol = em.merge(idRol);
            }
            if (idOpcionMenu != null) {
                idOpcionMenu.getRolOpcionMenuCollection().add(rolOpcionMenu);
                idOpcionMenu = em.merge(idOpcionMenu);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(RolOpcionMenu rolOpcionMenu) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            RolOpcionMenu persistentRolOpcionMenu = em.find(RolOpcionMenu.class, rolOpcionMenu.getId());
            Rol idRolOld = persistentRolOpcionMenu.getIdRol();
            Rol idRolNew = rolOpcionMenu.getIdRol();
            OpcionMenu idOpcionMenuOld = persistentRolOpcionMenu.getIdOpcionMenu();
            OpcionMenu idOpcionMenuNew = rolOpcionMenu.getIdOpcionMenu();
            if (idRolNew != null) {
                idRolNew = em.getReference(idRolNew.getClass(), idRolNew.getId());
                rolOpcionMenu.setIdRol(idRolNew);
            }
            if (idOpcionMenuNew != null) {
                idOpcionMenuNew = em.getReference(idOpcionMenuNew.getClass(), idOpcionMenuNew.getId());
                rolOpcionMenu.setIdOpcionMenu(idOpcionMenuNew);
            }
            rolOpcionMenu = em.merge(rolOpcionMenu);
            if (idRolOld != null && !idRolOld.equals(idRolNew)) {
                idRolOld.getRolOpcionMenuCollection().remove(rolOpcionMenu);
                idRolOld = em.merge(idRolOld);
            }
            if (idRolNew != null && !idRolNew.equals(idRolOld)) {
                idRolNew.getRolOpcionMenuCollection().add(rolOpcionMenu);
                idRolNew = em.merge(idRolNew);
            }
            if (idOpcionMenuOld != null && !idOpcionMenuOld.equals(idOpcionMenuNew)) {
                idOpcionMenuOld.getRolOpcionMenuCollection().remove(rolOpcionMenu);
                idOpcionMenuOld = em.merge(idOpcionMenuOld);
            }
            if (idOpcionMenuNew != null && !idOpcionMenuNew.equals(idOpcionMenuOld)) {
                idOpcionMenuNew.getRolOpcionMenuCollection().add(rolOpcionMenu);
                idOpcionMenuNew = em.merge(idOpcionMenuNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = rolOpcionMenu.getId();
                if (findRolOpcionMenu(id) == null) {
                    throw new NonexistentEntityException("The rolOpcionMenu with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            RolOpcionMenu rolOpcionMenu;
            try {
                rolOpcionMenu = em.getReference(RolOpcionMenu.class, id);
                rolOpcionMenu.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The rolOpcionMenu with id " + id + " no longer exists.", enfe);
            }
            Rol idRol = rolOpcionMenu.getIdRol();
            if (idRol != null) {
                idRol.getRolOpcionMenuCollection().remove(rolOpcionMenu);
                idRol = em.merge(idRol);
            }
            OpcionMenu idOpcionMenu = rolOpcionMenu.getIdOpcionMenu();
            if (idOpcionMenu != null) {
                idOpcionMenu.getRolOpcionMenuCollection().remove(rolOpcionMenu);
                idOpcionMenu = em.merge(idOpcionMenu);
            }
            em.remove(rolOpcionMenu);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<RolOpcionMenu> findRolOpcionMenuEntities() {
        return findRolOpcionMenuEntities(true, -1, -1);
    }

    public List<RolOpcionMenu> findRolOpcionMenuEntities(int maxResults, int firstResult) {
        return findRolOpcionMenuEntities(false, maxResults, firstResult);
    }

    private List<RolOpcionMenu> findRolOpcionMenuEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(RolOpcionMenu.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public RolOpcionMenu findRolOpcionMenu(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(RolOpcionMenu.class, id);
        } finally {
            em.close();
        }
    }

    public int getRolOpcionMenuCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<RolOpcionMenu> rt = cq.from(RolOpcionMenu.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
