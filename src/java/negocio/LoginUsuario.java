package negocio;

import entidades.Usuarios;
import entidadesDAO.UsuariosJpaController;

public class LoginUsuario {
    
    private Usuarios usuario;
    private final UsuariosJpaController usuarioDAO = new UsuariosJpaController();

    public LoginUsuario() {
    } 
        
    public boolean inicioSesion(String email, String password){
        usuario = usuarioDAO.findUsuarioByEmail(email);
        if (usuario != null) {
            return password.equals(usuario.getPassword());
        } 
        return false;
    }    
    
}
