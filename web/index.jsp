<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
        <title>JSP Page</title>
    </head>
    <body>

        <div class="container mt-5">
            
            <a class="btn btn-success" href="ControladorRol?accion=registrarOpcionesMenuARol">mostrar persona</a>
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-center">Iniciar sesión</h3>
                        </div>
                        <div class="card-body">
                            <!-- Formulario de inicio de sesión -->
                            <form action="ControladorUsuario">
                                <!-- Campo de nombre de usuario o correo electrónico -->
                                <div class="mb-3">
                                    <label for="username" class="form-label">Correo electrónico</label>
                                    <input type="text" class="form-control" id="username" name="username" required>
                                </div>

                                <!-- Campo de contraseña -->
                                <div class="mb-3">
                                    <label for="password" class="form-label">Contraseña</label>
                                    <input type="password" class="form-control" id="password" name="password" required>
                                </div>

                                <!-- Botón de inicio de sesión -->
                                <button type="submit" class="btn btn-primary w-100" name="accion" value="inicioSesion">Iniciar sesión</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Scripts de Bootstrap 5 (jQuery y Popper.js son necesarios para Bootstrap) -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>

    </body>
</html>
