<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <div class="col-lg-6">
                <br>
                <br>
                <h1>Agregar Usuario</h1>
                <form action="ControladorUsuario">
                    Tipo Documento<br>
                    <input class="form-control" type="text" name="txtTipoDocumento">
                    <br>
                    Numero Documentos<br>
                    <input class="form-control" type="text" name="txtNumeroDocumento">
                    <br>
                    Nombre:<br>
                    <input class="form-control" type="text" name="txtNombre">
                    <br>
                    Apellido:<br>
                    <input class="form-control" type="text" name="txtApellido">
                    <br>
                    Correo:<br>
                    <input class="form-control" type="text" name="txtCorreo">
                    <br>
                    Telefono:<br>
                    <input class="form-control" type="text" name="txtTelefono">
                    <br>
                    Password:<br>
                    <input class="form-control" type="text" name="txtPassword">
                    <br>
                    Rol:<br>
                    <input class="form-control" type="text" name="txtRol">
                    <br>
                    <br>
                    <input class="btn btn-primary" type="submit" name="accion" value="nuevo">
                </form>                
            </div>
        </div>
    </body>
</html>