<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="entidadesDAO.RolJpaController" %>
<%@ page import="entidades.Rol" %>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <div class="col-lg-6">
                <h1>Agregar Menús a Rol</h1>
                <form action="ControladorRol">
                    <br>
                    <select name="selectRol">
                        <option value="-1" selected>Elegir Rol</option>
                        <%
                            RolJpaController dao = new RolJpaController();
                            List<Rol> list = dao.findRolEntities();
                            Rol rol = null;
                    
                            Iterator<Rol> iter = list.iterator();

                            while (iter.hasNext()){
                                rol = iter.next();
                    
                        %>
                        <option name="option" value="<%= rol.getId()%>"><%= rol.getNombre()%></option>
                        <%}%>
                    </select>
                    <br>
                    Nombre de la ópcion menu: <br>
                    <input class="form-control" type="text" name="txtNombreOpcionMenu">
                    <br>
                    URL:<br>
                    <input class="form-control" type="text" name="txtUrl">
                    <br>
                    Icono: <br>
                    <input class="form-control" type="text" name="txtIcono">
                    <br>
                    <input class="btn btn-primary" type="submit" name="accion" value="agregarMenuARol">
                    <a href="ControladorPersonas?accion=mostrar">Regresar</a>
                </form> 
            </div>
    </body>
</html>